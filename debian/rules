#!/usr/bin/make -f
export DH_VERBOSE=1

MAKEFILE = $(firstword $(MAKEFILE_LIST))
DEBIAN_DIR = $(dir $(MAKEFILE))

SOURCE_DIR = $(DEBIAN_DIR)/..

DEB_VERSION = $(shell dpkg-parsechangelog | grep ^Vers | cut -d\  -f2)
DEB_REVISION = $(shell echo $(DEB_VERSION) | sed 's,.*-,,' )
VERSION = $(shell echo $(DEB_VERSION) | sed 's,-.*,,' | sed 's,+dfsg.*,,')
NEXT_UPVERSION = $(shell perl -e '$$_=pop; s/(\d+)$$/$$1+1/e; print' $(VERSION))
DEB_SOURCE_NAME = $(shell dpkg-parsechangelog -l$(DEBIAN_DIR)/changelog | grep ^Source | cut -d" " -f2)

IRONRUBY_VERSION = 0.9-$(DEB_REVISION)
IRONPYTHON_VERSION = 2.6~beta2-$(DEB_REVISION)
DEB_CLI_ABI_VERSION = 0.9
DEB_CLI_API_VERSION = 20090805+git.e6b28d27

include /usr/share/cli-common/cli.make
include /usr/share/quilt/quilt.make

override_dh_auto_build: $(QUILT_STAMPFN)
	for configuration in Debug;\
	do \
		for sln in Merlin/Main/Languages/Ruby/Ruby.sln \
			   Merlin/Main/Languages/IronPython/IronPython.sln ;\
		do \
			xbuild /p:TreatWarningsAsErrors=false \
			       /p:Configuration=$$configuration \
			       $$sln ;\
		done ;\
	done

# We ship the manpage to avoid a dependency on perl-doc just for this manpage.
# This is how you can build it if you update it.
debian/irake.1: debian/irake.pod
	perldoc debian/irake.pod | \
	perl -pe 's/(^\S+)\s*User Contributed Perl Documentation\s*(\S+)$$/$$1\t\t\t\t\t$$1/' | \
	head -n -1 > debian/irake.1

override_dh_clean: unpatch
	xbuild /t:Clean Merlin/Main/Languages/Ruby/Ruby.sln
	xbuild /t:Clean Merlin/Main/Languages/IronPython/IronPython.sln
	# clean it good
	rm -rf Merlin/Main/Bin/
	find Merlin/ ndp/ \
		\( -iname '*.exe' \
		-o -iname '*.dll' \
		-o -iname '*.pdb' \
		-o -iname '*.mdb' \
		-o -iname '*.resources' \) -delete
	rm -f Merlin/Main/Languages/IronPython/IronPython.sln.proj
	rm -f Merlin/Main/Languages/Ruby/Ruby.sln.proj
	dh_clean

override_dh_clistrip:
	dh_clistrip -p ironruby --dbg-package=ironruby-dbg
	dh_clistrip -p ironpython --dbg-package=ironpython-dbg

override_dh_makeclilibs:
	dh_makeclilibs -p libdlr0.9-cil -m $(DEB_CLI_API_VERSION)

override_dh_gencontrol:
	dh_gencontrol -p libdlr0.9-cil
	dh_gencontrol -p ironruby \
	              -p ironruby-dbg \
	              -p ironruby-utils \
	              -- -v$(IRONRUBY_VERSION) -Vironruby:Version=$(IRONRUBY_VERSION)
	dh_gencontrol -p ironpython \
	              -p ironpython-dbg \
	              -- -v$(IRONPYTHON_VERSION) -Vironpython:Version=$(IRONPYTHON_VERSION)

override_dh_python:
	# NOOP

get-orig-source: TARBALL_DIR = $(DEB_SOURCE_NAME)-$(VERSION)
get-orig-source:
	uscan \
		--package $(DEB_SOURCE_NAME) \
		--watchfile $(DEBIAN_DIR)/watch \
		--upstream-version $(VERSION) \
		--download-version $(VERSION) \
		--destdir . \
		--force-download \
		--rename \
		--repack
	mv $(VERSION) $(DEB_SOURCE_NAME)_$(VERSION).orig.tar.gz
	if [ -d $(TARBALL_DIR) ]; then \
		echo "$(TARBALL_DIR) is in the way, bailing out!"; \
		exit 1; \
	fi
	if [ -d $(TARBALL_DIR)+dfsg ]; then \
		echo "$(TARBALL_DIR)+dfsg is in the way, bailing out!"; \
		exit 1; \
	fi
	mkdir $(TARBALL_DIR)
	tar -C $(TARBALL_DIR) --strip-components=1 -xzf $(DEB_SOURCE_NAME)_$(VERSION).orig.tar.gz
	rm $(DEB_SOURCE_NAME)_$(VERSION).orig.tar.gz
	# Remove gems
	rm -rf $(TARBALL_DIR)/Merlin/External.LCA_RESTRICTED/Languages/Ruby
	# Remove wrapper scripts
	rm -f $(TARBALL_DIR)/Merlin/Main/Languages/IronPython/Scripts/ipy*
	# remove binaries
	find  $(TARBALL_DIR) \
		\( -iname '*.exe' \
		-o -iname '*.dll' \
		-o -iname '*.pdb' \
		-o -iname '*.mdb' \
		-o -iname '*.resources' \) \
		-delete
	mv $(TARBALL_DIR) $(TARBALL_DIR)+dfsg
	tar -czf $(DEB_SOURCE_NAME)_$(VERSION)+dfsg.orig.tar.gz $(TARBALL_DIR)+dfsg
	rm -r $(TARBALL_DIR)+dfsg

%:
	dh $@
